Permissions
###########

There's three levels of permissions in hkis:

- User: Can use the website normally.
- Teacher: A teacher is a staff (has access to `/admin/`) in the ``Teacher`` group.
- Superuser: It's a Django flag telling the user has all rights, this is for admins.


The Teacher Group
*****************

The Teacher group has the following Django permissions:

- View and change answers.
- Add, Change, View, and Delete exercises.

But hkis restricts Teachers a bit:

- A teacher can only view its own answers and the answers from users in the teams he own (by being staff in them).

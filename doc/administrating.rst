Administrating an HackInScience instance
========================================

So you just installed HackInScience what's next?


Creating an admin account
-------------------------

The first thing you'll need is an admin account, this is the only step
to do server-side on the command-line. It should be executed in the
root directory of HackInScience, where's `./manage.py` is::

    ./manage.py createsuperuser


Accessing the admin area
------------------------

Open your browser, go to your HackInScience website and login. You'll see an ``Admin`` link in the header.

It's often faster to just aim to `/admin/` manually though, once your browser knows it.


Creating mandatory pages
------------------------

Everything in HackInScience is stored in "pages": exercises, text, documentation, whatever.

So in the admin area, create at least two pages:

- `exercises` (or whatever name you want), you don't need to write text to it but you can, it'll store your exercises.
- `contact` this is a mandatory page as a few links points to them. Write down how to contact you in it.

Check the "in_menu" box to make them appear in the menu.

And you can adjust their « position » (page with lower position go to the left, higher position value go to the right).

The landing page for logged-in users is the first page from the menu.


Creating exercises
------------------

See :ref:`writing-exercises`.

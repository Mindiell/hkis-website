.. HKIS documentation master file, created by
   sphinx-quickstart on Sat Dec 11 10:56:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HKIS's documentation!
################################

HKIS is an MIT-licenced language-agnostic exercises platform with the
ability to give live feedback to students working on exercises.

You can test the main HKIS instance at https://www.hackinscience.org.


.. toctree::
   :maxdepth: 3

   teaching
   writing-exercises
   permissions
   running
   administrating
   contributing


Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

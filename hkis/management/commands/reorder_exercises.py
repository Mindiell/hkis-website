from django.core.management.base import BaseCommand
from rich.console import Console
from rich.table import Table

from hkis.models import Exercise


class Command(BaseCommand):
    help = "Reorder all exercises according to their number of solves."

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run", action="store_true", help="Just print what it would do."
        )

    def handle(self, *args, **options):
        console = Console(file=self.stdout)
        by_position = Exercise.objects.reorganize(save=not options["dry_run"])
        table = Table(title="Exercises")
        table.add_column("Position")
        table.add_column("Exercise")
        table.add_column("Solved (last 30 days)")
        table.add_column("Points")
        for pos, exercise in by_position:
            table.add_row(
                str(pos),
                exercise.title,
                str(exercise.last_month_successes),
                str(exercise.points),
            )
        console.print(table)

from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.reverse import reverse

from hkis.models import Answer, Exercise, Membership, Page, Tag, Team, User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            "url",
            "username",
            "first_name",
            "last_name",
            "email",
            "groups",
            "memberships",
        )

    memberships = serializers.SerializerMethodField()

    def get_memberships(self, obj):
        return (
            reverse("membership-list", request=self.context["request"])
            + f"?user={obj.id}"
        )


class SmallUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ("url", "username")


class OnlyStaffCanBumpRole:
    requires_context = True

    def __call__(self, value, serializer):
        membership = serializer.parent.instance
        user = serializer.context["request"].user

        if value != membership.role and not membership.team.is_staff(user):
            raise serializers.ValidationError("Not allowed to change role.")


class RoleField(serializers.Field):
    CONVERSION = {"MM": "member", "PE": "pending", "ST": "staff"}
    REVERSE_CONVERSION = {value: key for key, value in CONVERSION.items()}

    def to_representation(self, value):
        return self.CONVERSION[value]

    def to_internal_value(self, data):
        try:
            return self.REVERSE_CONVERSION[data]
        except ValueError as err:
            raise serializers.ValidationError(
                "Not a valid role, should be from: " + ", ".join(self.CONVERSION.keys())
            ) from err


class MembershipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Membership
        fields = ("url", "user", "team", "role")

    role = RoleField(validators=[OnlyStaffCanBumpRole()], default="PE")
    user = serializers.HyperlinkedRelatedField(
        queryset=User.objects.all(),
        required=True,
        view_name="user-detail",
        style={"base_template": "input.html"},
    )
    team = serializers.HyperlinkedRelatedField(
        queryset=Team.objects.all(),
        required=True,
        view_name="team-detail",
        style={"base_template": "input.html"},
    )


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ("url", "name")


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ("url", "slug", "title", "position")

    title = serializers.CharField(max_length=255, required=False)

    def to_internal_value(self, data):
        if isinstance(data, str):
            # Tags can be given as just a list of strings like
            # ["easy", "numpy"]
            data = {"slug": data}

        return super().to_internal_value(data)


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ("url", "slug", "title", "position")


class ExercisesOnPagesOwnerBySameOwner:
    requires_context = True

    def __call__(self, value, serializer_field):
        request_user = serializer_field.parent.context["request"].user
        if request_user.is_superuser:
            return
        exercise = serializer_field.parent.instance
        page = value
        if page.author != request_user:
            raise serializers.ValidationError(
                "Only the page author can assign exercises on its page."
            )
        if exercise:
            if exercise.author != page.author:
                raise serializers.ValidationError(
                    "The exercise author and the page owner have to be the same."
                )


class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Exercise
        fields = (
            "url",
            "title",
            "tags",
            "page",
            "html",
            "slug",
            "position",
            "points",
            "solved_by",
            "author",
        )
        read_only_fields = ("is_published", "author")

    tags = TagSerializer(many=True, default=[])
    html = serializers.SerializerMethodField()
    page = serializers.HyperlinkedRelatedField(
        queryset=Page.objects.all(),
        required=True,
        view_name="page-detail",
        validators=[ExercisesOnPagesOwnerBySameOwner()],
    )

    def get_html(self, obj):
        return self.context["request"].build_absolute_uri(obj.get_absolute_url())

    def get_or_create_tag(self, tag):
        """Given a tag we have multiples ways to seek it in order to reuse it.

        - By slug first, as it's the more stable indicator.
        - By title next.
        """
        if tag.get("slug"):
            tag_instance, _created = Tag.objects.update_or_create(
                slug=tag["slug"],
                defaults=tag,
                create_defaults={"title": tag["slug"].capitalize()} | tag,
            )
            return tag_instance
        if tag.get("title"):
            tag_instance, _created = Tag.objects.update_or_create(
                title=tag["title"], defaults=tag
            )
            return tag_instance
        # We could also search it by pk as we may have its 'url' :D
        raise ValueError(
            "A tag should have at least a title or a slug in order for us to find it or create it"
        )

    def get_or_create_tags(self, tags):
        tags_ids = []
        for tag in tags:
            tag_instance = self.get_or_create_tag(tag)
            tags_ids.append(tag_instance.pk)
        return tags_ids

    def create(self, validated_data):
        """Nested tag handling.

        Tags are always reused when possible, it does not depend on if
        we're creating or updating an exercise.
        """
        tags = validated_data.pop("tags", [])
        instance = self.Meta.model.objects.create(**validated_data)
        instance.tags.set(self.get_or_create_tags(tags))
        return instance

    def update(self, instance, validated_data):
        """Nested tag handling.

        Tags are always reused when possible, it does not depend on if
        we're creating or updating an exercise.
        """
        tags = validated_data.pop("tags", [])
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        instance.tags.set(self.get_or_create_tags(tags))
        return instance


class ExerciseSerializerFull(ExerciseSerializer):
    class Meta:
        model = Exercise
        fields = ExerciseSerializer.Meta.fields + (
            "wording",
            "is_published",
            "check_py",
            "initial_solution",
            "created_at",
        )


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer
        fields = "__all__"
        read_only_fields = (
            "user",
            "is_corrected",
            "is_valid",
            "correction_message",
            "created_at",
            "corrected_at",
        )


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = ("url", "name", "slug", "created_at", "points", "memberships", "page")
        read_only_fields = ("points", "created_at", "slug")

    memberships = serializers.SerializerMethodField()
    page = serializers.SerializerMethodField()

    def get_page(self, obj):
        return self.context["request"].build_absolute_uri(obj.get_absolute_url())

    def get_memberships(self, team):
        return (
            reverse("membership-list", request=self.context["request"])
            + f"?team={team.id}"
        )


class SmallTeamSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = ("url", "name")

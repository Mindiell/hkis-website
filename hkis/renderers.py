from rest_framework import renderers


class BrowsableAPIRenderer(renderers.BrowsableAPIRenderer):
    def show_form_for_method(self, view, method, request, obj):
        return False

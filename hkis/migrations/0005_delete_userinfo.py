# Generated by Django 5.0.6 on 2024-10-07 19:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("hkis", "0004_alter_user_managers_user_points_user_public_profile_and_more"),
    ]

    operations = [
        migrations.DeleteModel(
            name="UserInfo",
        ),
    ]

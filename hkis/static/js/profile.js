window.addEventListener("DOMContentLoaded", function (event) {
    const settings = document.getElementsByTagName('body')[0].dataset;

    // Configure button to set language.
    document.getElementById("language").addEventListener("click", e => {
        const language = document.getElementById("language-select").value;
        document.cookie = `django_language=${language};samesite=strict;max-age=31536000;path=/`;
        location.reload();
    });

    // Configure button to join team.
    document.getElementById("join-team").addEventListener("click", async e => {
        const teamId = document.getElementById("join-team-select").value;
        const team = await api_get(`/api/teams/${teamId}`);
        const membership = await api_post(
            "/api/memberships/",
            {"team": team["url"], "user": settings.apiCurrentUserUrl}
        );
        location.reload();
    });

    // Configure button to create team.
    document.getElementById("create-team").addEventListener("click", async e => {
        const teamName = document.getElementById("create-team-input").value;
        const team = await api_post("/api/teams/", {"name": teamName});
        const membership = await api_post(
            "/api/memberships/",
            {"team": team["url"], "user": settings.apiCurrentUserUrl}
        );
        location.reload();
    });

    // Configure buttons to leave team.
    document.querySelectorAll("button[data-api-leave-membership-url]").forEach(button => {
        button.addEventListener("click", async e => {
            const membershipUrl = e.target.dataset.apiLeaveMembershipUrl;
            await api_delete(membershipUrl);
            location.reload();
        });
    });

    // Use select2 for team select.
    $('#join-team-select').select2();
});

window.addEventListener("DOMContentLoaded", function (event) {
    const form = document.getElementById('registration_form');
    const button = document.getElementById('registration_button');
    form.addEventListener('submit', function(event) {
        if (button.disabled)
            return false;
        button.disabled = true;
    });
});

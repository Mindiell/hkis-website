window.addEventListener("DOMContentLoaded", function (event) {
    const remove_from_team = async (event) => {
        const membership_url = event.target.dataset.apiRemoveMembershipUrl;
        await api_delete(membership_url);
        location.reload(); // I'm too lazy to update the DOM client-side, let Django template work.
    }

    document.querySelectorAll("button[data-api-remove-membership-url]").forEach(function (button) {
        button.addEventListener("click", remove_from_team);
    });


    const accept_in_team = async (event) => {
        const membership_url = event.target.dataset.apiAcceptInTeamUrl;
        membership = await api_get(membership_url);
        membership.role = "member";
        await api_put(membership.url, membership);
        location.reload(); // I'm too lazy to update the DOM client-side, let Django template work.
    }

    document.querySelectorAll("button[data-api-accept-in-team-url]").forEach(function (button) {
        button.addEventListener("click", accept_in_team);
    });
});

window.addEventListener("DOMContentLoaded", function (event) {
    var admin_debug_toolbar = document.getElementById("admin_switch_debug_toolbar");
    if (admin_debug_toolbar) {
        var secret = admin_debug_toolbar.dataset.debugToolbarSecret;
        var is_enabled = document.cookie.indexOf(secret) >= 0;
        console.log("Debug toolbar is enabled:", is_enabled);
        admin_debug_toolbar.checked = is_enabled;
        admin_debug_toolbar.addEventListener("click", function(e) {
            if (is_enabled)
                document.cookie = "debug_toolbar_secret=;path=/";
            else
                document.cookie = "debug_toolbar_secret=" + secret + ";path=/";
            location.reload();
        });
    }
});

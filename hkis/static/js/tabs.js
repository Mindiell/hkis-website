function displayTab(tabId) {
    if (typeof tabId === "undefined")
        // Pick first tab name.
        tabId = document.querySelector(".tab").id;
    else
        // Unselect all tabs.
        document.querySelectorAll(".tabs > li, .tab").forEach(item => {
            item.classList.remove("selected");
        });

    // Select tab.
    document.querySelectorAll(`.tabs > li.${tabId}, #${tabId}`).forEach(item => {
        item.classList.add("selected");
    });
}

window.addEventListener("DOMContentLoaded", event => {
    // Display first tab and link buttons to tabs.
    displayTab();
    document.querySelectorAll(".tab").forEach(item => {
        var tab = document.querySelector(`.tabs li.${item.id}`);
        if (tab === null) return;
        tab.addEventListener("click", e => {
            displayTab(item.id);
        });
    });
});

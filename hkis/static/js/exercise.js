var hkis = {
    createElement: function(tagName, attrs, dataset) {
        el = document.createElement(tagName)
        for (const property in attrs)
            el[property] = attrs[property]
        if (typeof dataset != "undefined")
            for (const property in dataset)
                el.dataset[property] = dataset[property]
        return el
    }
}

function appendToAnswerTable(html) {
    if (document.querySelectorAll("#answer-table tr").length)
        document.querySelectorAll("#answer-table tr")[0].insertAdjacentHTML("beforebegin", html);
    else
        document.querySelectorAll("#answer-table")[0].insertAdjacentHTML("afterbegin", html);
}

/*
function report_as_unhelpfull(answer_id) {
    if (confirm(gettext("This will send this answer to a human for manual review, so it can be enhanced, are you sure?")))
        window.ws.send(JSON.stringify({type: "is_unhelpfull", answer_id: answer_id}));
    return false;
}
*/

function rankUpdated(newRank) {
    var messageList = document.querySelector("#correction .messages");
    displayTab("correction");

    console.log("Answer is valid: old_rank=", settings.currentRank, "new_rank=", newRank)

    if (newRank < parseInt(settings.currentRank)) {
        messageList.appendChild(hkis.createElement("li", {
            className: "success",
            innerHTML: interpolate(
                gettext('Your new <a href="%(url)s">rank</a> is: %(newRank)s'),
                {url: settings.leaderboardUrl, newRank: newRank},
              true
            )
        }));
    }
}

function fillAnswer(answer) {
    var messageList = document.querySelector("#correction .messages");
    displayTab("correction");

    if (!answer.is_corrected) {
        messageList.replaceChildren(hkis.createElement("li", {
            className: "info",
            innerHTML: gettext("Waiting for correction...")
        }));
        return;
    }

    var messages = [];
    var answerHTML = answer.correction_message_html;
    if (answerHTML.indexOf("admonition") == -1) {
        // If the correction bot did not provided admonitions, let's make it ourself.
        messages.push(hkis.createElement("li", {
            className: "info",
            innerHTML: answerHTML
        }));
    } else {
        // Parse and adapt given admonitions.
        var wrapper = document.createElement("div");
        wrapper.innerHTML = answerHTML;
        Array.from(wrapper.children).forEach(item => {
            messages.push(
                hkis.createElement("li", {className: item.className, innerHTML: item.innerHTML})
            );
        })
    }

    if (answer.is_valid) {
        // Highlight "Next exercise" button.
        document.querySelector("#buttons .next").classList.add("highlight");

        // Warn anonymous users.
        if (!answer.user) {
            messages.push(hkis.createElement("li", {
                className: "warning",
                innerHTML: interpolate(
                    gettext('<a href="%s">Login</a> to backup your code and progression.'),
                    [settings.authLoginUrl]
                )
            }));
        }

        // Allow user to see solutions.
        document.getElementById("solution-link").classList.remove("disabled");
    }
    else {
        /*
        var button_unhelpfull = hkis.createElement("button", {
            title: gettext("Flag this answer as particularly unhelpfull,\nIt'll be reviewed by a human."),
            className: "btn, btn-outline-danger btn-sm",
            onclick: function() {return report_as_unhelpfull(answer.id);},
            innerHTML: gettext("Report as unhelpfull")
        }, {
            toggle: "tooltip"
        });

        if (answer.is_unhelpfull) {
            button_unhelpfull.disabled = true;
            button_unhelpfull.innerHTML = gettext("Reported");
            button_unhelpfull.title = gettext("Thanks for the feedback!");
        }
        div.appendChild(button_unhelpfull);
       */
    }
    messageList.replaceChildren(...messages);
    unlockButton("submit-answer");
}

function fillMessage(message, type) {
    // type is in {"success", "danger", "warning", "info"}.
    console.log("[" + type + "]" + message);
    var answerTable = document.getElementById("answer-table");
    if (!answerTable) return;
    var firstRow = answerTable.getElementsByTagName("tr")[0];
    var firstCell = null;
    if (firstRow)
        firstCell = firstRow.getElementsByTagName("td")[0];
    var html = `<div class="alert alert-${type}">${message}</div>`;
    if (firstCell && !firstCell.id)
        firstCell.innerHTML = html;
    else
        appendToAnswerTable(`<tr><td>${html}</td></tr>`);
}

function websocketConnect(wsLocation) {
    var connected = false;
    fillMessage(gettext("Connecting to correction server…"), "info");
    window.ws = new WebSocket(wsLocation);
    window.ws.onmessage = function(e) {
        var data = JSON.parse(e.data);
        console.log("WebSocket recv:", data);
        if (data.type == "answer.update")
            fillAnswer(data);
        if (data.type == "rank.updated")
            rankUpdated(data.new_rank);
    };
    window.ws.onerror = function(event) {
        console.error("WebSocket error observed:", event);
    };
    window.ws.onopen = function() {
        unlockButton("submit-answer");
        connected = true;
        fillMessage(gettext("Connected to correction server."), "success");
        window.ws.send(JSON.stringify({type: "settings", value: {LANGUAGE_CODE: settings.languageCode}}));
        document.querySelectorAll("td.answer-cell").forEach(function(answer) {
            if (answer.dataset.isCorrected == "True") return;
            console.log("Need to correct answer number", answer.dataset.answerId);
            window.ws.send(JSON.stringify({"type": "recorrect", "id": Number(answer.dataset.answerId)}));
        });
    };
    window.ws.onclose = function(event) {
        console.log("onclose", event);
        if (connected)
            fillMessage(gettext("Connection to correction server lost, will retry in 5s…"), "warning");
        else
            fillMessage(gettext("Cannot connect to correction server, will retry in 5s…"), "warning");
        setTimeout(function(){websocketConnect(wsLocation)}, 5000);
        lockButton("submit-answer", 0);
    };
}

function wsSubmitAnswer(form) {
    var message = {"type": "answer", "source_code": form["source_code"].value};
    lockButton("submit-answer", 1);
    console.log("WebSocket send answer", message);
    window.ws.send(JSON.stringify(message));
}

function unlockButton(buttonId) {
    document.getElementById(buttonId).removeAttribute("disabled");
}

function lockButton(buttonId, unlockAfterSeconds) {
    document.getElementById(buttonId).setAttribute("disabled", true);
    if (unlockAfterSeconds)
        setTimeout(() => {unlockButton(buttonId)}, unlockAfterSeconds * 1000);
}

function buttonIsLocked(buttonId) {
    return document.getElementById(buttonId).hasAttribute("disabled");
}

function shortcutsHandler(event) {
    if (event.ctrlKey && event.code == "Enter") {
        if (buttonIsLocked("submit-answer")) {
            console.log("Enhance your calm.");
            return;
        }
        console.log("Sending via Ctrl-enter");
        wsSubmitAnswer(document.getElementById("answer-form"));
    } else if (event.code == "Escape") {
        displayTab("instructions");
    }
}

var wsProtocol = window.location.protocol == "http:" ? "ws:" : "wss:";

window.addEventListener("DOMContentLoaded", function (event) {
    settings = document.getElementsByTagName("body")[0].dataset;

    websocketConnect(`${wsProtocol}//${window.location.host}/ws/exercises/${settings.exerciseId}/`);
    document.addEventListener("keydown", shortcutsHandler);

    document.getElementById("submit-answer").addEventListener("click", e => {
        e.preventDefault();
        wsSubmitAnswer(document.getElementById("answer-form"));
        setTimeout(() => window.scrollTo({top: 0, behavior: "smooth"}), 200);
        return false;
    });
    if (settings.isValid == "False") {
        document.getElementById("solution-link").classList.add("disabled");
    }
})

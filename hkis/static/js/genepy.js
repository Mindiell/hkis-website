const CSRF_TOKEN = document.cookie
      .split("; ")
      .find((row) => row.startsWith("csrftoken="))
      ?.split("=")[1];

const api_ = async (url, body, method) => {
    const response = await fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-CSRFToken": CSRF_TOKEN,
        }});
    if (response.headers.get("Content-Type") == "application/json")
        return await response.json();
};

const api_post = async (url, body) => { return await api_(url, body, "POST"); };
const api_put = async (url, body) => { return await api_(url, body, "PUT"); };
const api_delete = async (url, body) => { return await api_(url, body, "DELETE"); };

const api_get = async (url, body) => {
    const response = await fetch(url, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }});
    return await response.json();
};

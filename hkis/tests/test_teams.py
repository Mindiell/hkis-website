from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.test import APITestCase

from hkis.models import Team

User = get_user_model()


class TestTeams(TestCase):
    fixtures = ["initial"]

    def test_public_teams(self):
        assert User.objects.get(username="a-teacher").public_teams()

    def test_team_by_rank(self):
        assert list(Team.objects.first().members_by_rank())


class TestAPITeamAsAnon(APITestCase):
    def setUp(self):
        self.team = Team.objects.create(name="blue")
        self.team = Team.objects.create(name="red")

    def test_anon_can_list_teams(self):
        response = self.client.get("/api/teams/")
        assert response.status_code == 200
        assert len(response.data["results"]) == 2
        assert response.data["results"][0]["name"] == "blue"
        assert response.data["results"][1]["name"] == "red"

    def test_anon_can_search_team_by_name(self):
        response = self.client.get("/api/teams/?q=blue")
        assert response.status_code == 200
        assert len(response.data["results"]) == 1
        assert response.data["results"][0]["name"] == "blue"

        response = self.client.get("/api/teams/?q=red")
        assert response.status_code == 200
        assert len(response.data["results"]) == 1
        assert response.data["results"][0]["name"] == "red"

    def test_anon_cannot_create_team(self):
        response = self.client.post("/api/teams/", {"name": "pwn3d"})
        assert response.status_code == 403


class TestAPITeamAsUser(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", "user@example.com", "pass")

    def test_one_can_create_new_teams(self):
        self.client.force_authenticate(user=self.user)
        me = self.client.get("/api/me/")

        team = self.client.post("/api/teams/", {"name": "red"})
        assert team.status_code == 201
        assert team.data["name"] == "red"
        assert Team.objects.get(name="red")

        # While we're here check we're member of the created team:
        for point_of_view in me, team:
            memberships_link = point_of_view.data["memberships"]
            memberships = self.client.get(memberships_link).data["results"]
            assert len(memberships) == 1
            membership = memberships[0]
            assert membership["user"] == me.data["url"]
            assert membership["team"] == team.data["url"]
            assert membership["role"] == "staff"

        # While we're here check we can delete the team
        team_deletion = self.client.delete(team.data["url"])
        assert team_deletion.status_code == 204
        assert Team.objects.count() == 0
        assert self.client.get(team.data["url"]).status_code == 404


class TestAPITeamCannotBeDeletedByRandoms(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", "user@example.com", "pass")
        self.team = Team.objects.create(name="blue")

    def test_one_cannot_delete_other_team(self):
        self.client.force_authenticate(user=self.user)
        team = self.client.get("/api/teams/").data["results"][0]
        assert self.client.delete(team["url"]).status_code == 403
        assert Team.objects.count() == 1


class TestAPITeamCanBeJoined(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", "user@example.com", "pass")
        self.team = Team.objects.create(name="blue")

    def test_one_can_ask_to_join_teams(self):
        self.client.force_authenticate(user=self.user)
        me = self.client.get("/api/me/")
        teams = self.client.get("/api/teams/").data["results"]
        team = self.client.get(teams[0]["url"])  # For symmetry with 'me'

        membership = self.client.post(
            "/api/memberships/", {"team": team.data["url"], "user": me.data["url"]}
        )
        assert membership.status_code == 201

        # Now test that we're actually waiting in the team from both points of view:
        for point_of_view in me, team:
            memberships_link = point_of_view.data["memberships"]
            memberships = self.client.get(memberships_link).data["results"]
            assert len(memberships) == 1
            membership = memberships[0]
            assert membership["user"] == me.data["url"]
            assert membership["team"] == team.data["url"]
            assert membership["role"] == "pending"


class TestAPITeamValidationAsUser(APITestCase):
    """Here we're ensuring that STAFF can change member roles.

    It's allowed, for staff, to change role PENDING to MEMBERS to STAFF.
    It's **not** allowed for PENDING to become member by themself.
    """

    def setUp(self):
        self.user = User.objects.create_user("user", "user@example.org", "pass")
        self.owner = User.objects.create_user("owner", "owner@example.org", "pass")
        self.team = Team.objects.create(name="A team")
        self.team.add_member(self.owner)

    def join_the_team(self):
        """I can join a team with role PENDING but not MEMBER."""
        self.client.force_authenticate(user=self.user)
        me = self.client.get("/api/me/")
        team = self.client.get("/api/teams/1/")

        # Let's join the team:
        membership = self.client.post(
            "/api/memberships/", data={"user": me.data["url"], "team": team.data["url"]}
        )
        assert membership.status_code == 201
        assert membership.data["role"] == "pending"

    def accept_user_in_team(self):
        self.client.force_authenticate(user=self.owner)
        me = self.client.get("/api/me/")
        my_membership = self.client.get(me.data["memberships"])
        my_team = self.client.get(my_membership.data["results"][0]["team"])
        memberships = self.client.get(my_team.data["memberships"])
        my_team_wait_list = [
            membership
            for membership in memberships.data["results"]
            if membership["role"] == "pending"
        ]
        subscription = self.client.get(my_team_wait_list[0]["url"])
        assert subscription.status_code == 200

        # Accept it as member:
        assert subscription.data["role"] == "pending"
        subscription.data["role"] = "member"
        subscription = self.client.put(subscription.data["url"], subscription.data)
        assert subscription.status_code == 200
        assert subscription.data["role"] == "member"

        # Double check we're member:
        subscription = self.client.get(subscription.data["url"])
        assert subscription.status_code == 200
        assert subscription.data["role"] == "member"

        # Bump member to staff:
        assert subscription.data["role"] == "member"
        subscription.data["role"] = "staff"
        subscription = self.client.put(subscription.data["url"], subscription.data)
        assert subscription.status_code == 200
        assert subscription.data["role"] == "staff"

        # Double check we're staff:
        subscription = self.client.get(subscription.data["url"])
        assert subscription.status_code == 200
        assert subscription.data["role"] == "staff"

    def test_try_to_join_team_as_member(self):
        self.join_the_team()

        me = self.client.get("/api/me/").data
        memberships = self.client.get(me["memberships"]).data
        membership = memberships["results"][0]

        # Now try modify own membership, it should not be possible:
        membership["role"] = "member"
        result = self.client.put(membership["url"], data=membership)
        assert result.status_code == 403

        # Double check if we're still having the 'pending' role:
        assert self.client.get(membership["url"]).data["role"] == "pending"

    def test_staff_accepts_user(self):
        self.join_the_team()
        self.accept_user_in_team()

    def test_leave_team_while_pending(self):
        self.join_the_team()
        me = self.client.get("/api/me/").data

        # Before we have 1 pending membership:
        memberships = self.client.get(me["memberships"])
        assert len(memberships.data["results"]) == 1
        assert memberships.data["results"][0]["role"] == "pending"

        # We delete it:
        response = self.client.delete(memberships.data["results"][0]["url"])
        assert response.status_code == 204

        # Now we have 0 memberships:
        memberships = self.client.get(me["memberships"])
        assert len(memberships.data["results"]) == 0

    def test_leave_team_as_member(self):
        self.join_the_team()
        self.accept_user_in_team()

        me = self.client.get("/api/me/").data

        # Before we have 1 membership:
        memberships = self.client.get(me["memberships"])
        assert len(memberships.data["results"]) == 1
        assert memberships.data["results"][0]["role"] == "staff"

        # We delete it:
        response = self.client.delete(memberships.data["results"][0]["url"])
        assert response.status_code == 204

        # Now we have 0 memberships:
        memberships = self.client.get(me["memberships"])
        assert len(memberships.data["results"]) == 0


class TestAPITeamStaffPromotion(APITestCase):
    """When the last staff leaves a teaam, a random member
    should automatically be promoted."""

    def setUp(self):
        self.user = User.objects.create_user("user", "user@example.com", "pass")
        self.owner = User.objects.create_user("owner", "owner@example.com", "pass")
        self.team = Team.objects.create(name="A team")
        self.team.add_member(self.owner)  # First one gets 'staff'
        self.team.add_member(self.user)  # Second one gets 'pending'

    def test_setup_properly(self):
        """Ensure the team has one owner, one member."""
        teams = self.client.get("/api/teams/")
        team_url = teams.data["results"][0]["url"]
        team = self.client.get(team_url)
        memberships = self.client.get(team.data["memberships"])
        assert len(memberships.data["results"]) == 2
        roles = {membership["role"] for membership in memberships.data["results"]}
        assert "staff" in roles
        assert "pending" in roles

    def test_removing_staff_promotes_someone(self):
        self.client.force_authenticate(user=self.owner)
        me = self.client.get("/api/me/")
        memberships = self.client.get(me.data["memberships"])
        membership = memberships.data["results"][0]

        # First let's remove the staff:
        response = self.client.delete(membership["url"])
        assert response.status_code == 204

        # Now check if the user got promoted:
        self.client.force_authenticate(user=self.user)
        me = self.client.get("/api/me/")
        memberships = self.client.get(me.data["memberships"])
        membership = memberships.data["results"][0]
        assert membership["role"] == "staff"


class TestAPITeamEmptyTeamGetsDropped(APITestCase):
    """When the last staff leaves a teaam, a random member
    should automatically be promoted."""

    def setUp(self):
        self.owner = User.objects.create_user("owner", "owner@example.com", "pass")
        self.team = Team.objects.create(name="A team")
        self.team.add_member(self.owner)  # First one gets 'staff'

    def test_removing_last_member_drops_team(self):
        self.client.force_authenticate(user=self.owner)
        me = self.client.get("/api/me/")
        memberships = self.client.get(me.data["memberships"])
        membership = memberships.data["results"][0]

        # First let's remove the staff:
        response = self.client.delete(membership["url"])
        assert response.status_code == 204

        # Now check the team has been deleted:
        assert Team.objects.count() == 0

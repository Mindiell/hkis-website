from django.test import TestCase

from hkis.models import Tag


class TestTags(TestCase):
    fixtures = ["initial"]

    def test_unnamed_tag(self):
        c1 = Tag.objects.create()
        assert str(c1) == "Unnamed"

    def test_named_tag(self):
        assert str(Tag.objects.first()) != "Unnamed"

import django.contrib.auth.backends
from django.contrib.auth import get_user_model
from django.db.models import Q

User = get_user_model()


class ModelBackend(django.contrib.auth.backends.ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None:
            username = kwargs.get(User.USERNAME_FIELD)
        if username is None or password is None:
            return None
        have_to_reduce_timing = True
        for user in User.objects.filter(Q(username=username) | Q(email=username)):
            have_to_reduce_timing = False
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
        if have_to_reduce_timing:
            User().set_password(password)
        return None

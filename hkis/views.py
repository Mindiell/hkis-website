from collections import defaultdict
from contextlib import suppress

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.translation import gettext
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from rest_framework.reverse import reverse as api_reverse

from hkis.forms import AnswerForm
from hkis.models import Answer, Exercise, Membership, Page, Tag, Team, User


def index(request):
    return render(
        request,
        "hkis/index.html",
        {
            "exercises": (
                Exercise.objects.filter(is_published=True)
                .select_related("author")
                .only("title", "tags", "page", "slug", "solved_by", "author")
            ),
            "tags": (
                Tag.objects.filter(exercises__is_published=True)
                .prefetch_related("exercises")
                .distinct()
            ),
            "answers": (Answer.objects.filter(is_shared=True).only("id")),
            "individual_leaders": (
                User.with_rank.filter(rank__isnull=False).only(
                    "first_name", "last_name", "username", "points"
                )[:3]
            ),
            "team_leaders": Team.objects.only("name").order_by("-points")[:3],
        },
    )


class ProfileView(LoginRequiredMixin, UpdateView):
    model = User
    fields = ["username", "email"]
    template_name = "hkis/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["user_info"] = User.with_rank.get(pk=self.request.user.pk)
        context["memberships"] = self.request.user.memberships.all()
        context["done_qty"] = (
            Answer.objects.filter(user=self.request.user, is_valid=True)
            .values_list("exercise_id")
            .distinct()
            .count()
        )
        context["submit_qty"] = Answer.objects.filter(user=self.request.user).count()
        context["participants"] = User.objects.count()
        context["languages"] = settings.LANGUAGES
        context["api_current_user_url"] = api_reverse(
            "user-detail", args=[self.request.user.pk], request=self.request
        )
        rank = context["user_info"].rank
        if context["user_info"].rank is not None:
            context["leaderboard"] = (
                User.with_rank.order_by("rank")
                .filter(rank__isnull=False)
                .prefetch_related("teams")[max(0, rank - 1 - 5) : rank - 1 + 5]
            )
        context["teams"] = Team.objects.order_by("name")

        return context

    def dispatch(self, request, *args, **kwargs):
        if kwargs["pk"] != request.user.pk:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        messages.info(self.request, "Profile updated")
        return reverse("profile", kwargs={"pk": self.request.user.id})


class Leaderboard(ListView):
    queryset = User.with_rank.filter(rank__isnull=False).prefetch_related("teams")
    paginate_by = 100
    template_name = "hkis/leaderboard.html"
    ordering = ["-points"]


class TeamLeaderboard(ListView):
    queryset = Team.objects.filter(is_public=True).prefetch_related("members")
    paginate_by = 100
    template_name = "hkis/team_leaderboard.html"
    ordering = ["-points"]


class ExercisesView(DetailView):
    model = Page
    slug_url_kwarg = "exercises"
    template_name = "hkis/exercises.html"

    def flag_by_difficulty(self, exercises):
        by_solved = sorted(exercises, key=lambda e: e.solved_by)
        pct = len(by_solved) / 100
        easy = int(25 * pct)
        medium = int(25 * pct + 40 * pct)
        hard = int(25 * pct + 40 * pct + 25 * pct)
        for exercise in exercises[:easy]:
            exercise.difficulty = "easy"
        for exercise in exercises[easy:medium]:
            exercise.difficulty = "medium"
        for exercise in exercises[medium:hard]:
            exercise.difficulty = "hard"
        for exercise in exercises[hard:]:
            exercise.difficulty = "very-hard"

    def get_exercises(self, page):
        return list(
            page.exercises.filter(is_published=True)
            .select_related("author", "page")
            .prefetch_related("tags")
            .only("title", "tags", "page", "slug", "solved_by", "author", "created_at")
        )

    def gather_tags(self, exercises) -> dict[str, Tag]:
        tags = {}
        nb_exercises: dict[str, int] = {}
        for exercise in exercises:
            for tag in exercise.tags.all():
                tags[tag.slug] = tag
                nb_exercises[tag.slug] = nb_exercises.get(tag.slug, 0) + 1
        for tag in tags.values():
            tag.nb_exercises = nb_exercises[tag.slug]
        return tags

    def get_exercises_done(self, selected_tag: Tag | None):
        request = self.request
        done = Answer.objects.filter(user=request.user, is_valid=True).distinct()
        failed = Answer.objects.filter(user=request.user, is_valid=False).distinct()
        if selected_tag is not None:
            done = done.filter(exercise__tags=selected_tag)
            failed = failed.filter(exercise__tags=selected_tag)
        done = {row[0] for row in done.values_list("exercise_id")}
        failed = {row[0] for row in failed.values_list("exercise_id")}
        return done, failed

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data()
        request = self.request
        resolved = None
        with suppress(ValueError):
            resolved = int(request.GET.get("resolved", ""))
        page = context["object"]
        exercises = self.get_exercises(page)
        have_exercises = bool(len(exercises))
        self.flag_by_difficulty(exercises)
        tags = self.gather_tags(exercises)
        if selected_tag := tags.get(request.GET.get("tag", "")):
            exercises = [e for e in exercises if selected_tag in e.tags.all()]
            context["title"] = selected_tag.title
            context["body"] = selected_tag.description
        else:
            selected_tag = None
            context["title"] = page.title
            context["body"] = page.body
        done: set[int] = set()
        failed: set[int] = set()
        todo: set[int] = set()
        next_exercise = None
        if not request.user.is_anonymous:
            done, failed = self.get_exercises_done(selected_tag)
            todo = {exercise.id for exercise in exercises if exercise.id not in done}
            next_exercise = next((e for e in exercises if e.id not in done), None)
        if resolved is not None:
            if resolved:
                exercises = [e for e in exercises if e.id in done]
            else:
                exercises = [e for e in exercises if e.id in todo]
        context["total"] = len(exercises)
        return context | {
            "resolved": resolved,
            "exercises_done": done,
            "exercises_todo": todo,
            "exercises_failed": failed,
            "next_exercise": next_exercise,
            "selected_tag": selected_tag,
            "exercises": exercises,
            "tags": tags.values(),
            "have_exercises": have_exercises,
        }


class ExerciseView(DetailView):
    model = Exercise
    template_name = "hkis/exercise.html"

    def get_object(self, queryset=None):
        """Return the object the view is displaying."""
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(
            page__slug=self.kwargs["exercises"], slug=self.kwargs["exercise"]
        )
        try:
            # Get the single item from the filtered queryset
            return queryset.get()
        except queryset.model.DoesNotExist as err:
            raise Http404("No exercise found matching the query") from err

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.select_related("author")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["LANGUAGE_CODE"] = self.request.LANGUAGE_CODE
        user = self.request.user
        last_answer = None
        if not user.is_anonymous:
            last_answer = self.object.answers.filter(user=user).order_by("-id").first()
        context["answer_form"] = AnswerForm(
            language=self.object.page.language,
            initial={
                "exercise": f"/api/exercises/{self.object.id}/",
                "source_code": (
                    last_answer.source_code
                    if last_answer
                    else context["exercise"].initial_solution
                ),
            },
        )
        context["object"].wording = gettext(context["object"].wording)
        try:
            context["current_rank"] = User.with_rank.get(pk=user.pk).rank
        except (User.DoesNotExist, TypeError):
            context["current_rank"] = 999_999
        if user.is_anonymous:
            context["is_valid"] = False
        else:
            context["is_valid"] = bool(
                self.object.answers.filter(user=user, is_valid=True)
            )
        try:
            context["next"] = Exercise.objects.filter(
                page=self.object.page,
                is_published=True,
                position__gt=self.object.position,
            ).order_by("position")
        except IndexError:
            context["next"] = None
        try:
            context["previous"] = Exercise.objects.filter(
                is_published=True,
                page=self.object.page,
                position__lt=self.object.position,
            ).order_by("-position")
        except IndexError:
            context["previous"] = None
        return context


class SolutionView(LoginRequiredMixin, DetailView):
    model = Exercise
    template_name = "hkis/solutions.html"

    def get_object(self, queryset=None):
        """Return the object the view is displaying."""
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(
            page__slug=self.kwargs["page"], slug=self.kwargs["exercise"]
        )
        try:
            # Get the single item from the filtered queryset
            return queryset.get()
        except queryset.model.DoesNotExist as err:
            raise Http404("No exercise found matching the query") from err

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["my_answers"] = self.object.answers.filter(
            user=self.request.user
        ).order_by("created_at")
        if self.object.is_solved_by(self.request.user):
            context["is_solved"] = True
            context["solutions"] = self.object.shared_solutions()[:10]
        else:
            context["is_solved"] = False
            context["solutions"] = []
        try:
            context["next"] = (
                Exercise.objects.filter(position__gt=self.object.position)
                .order_by("position")[0]
                .slug
            )
        except IndexError:
            context["next"] = None
        return context


def team_stats(request, slug):
    try:
        team = Team.objects.get(slug=slug)
    except Team.DoesNotExist as err:
        raise Http404("Team does not exist") from err

    requester_membership = None
    if not request.user.is_anonymous:
        with suppress(Membership.DoesNotExist):
            requester_membership = Membership.objects.get(team=team, user=request.user)
    if not requester_membership:
        raise Http404("Team does not exist")
    if requester_membership.role != Membership.Role.STAFF:
        raise Http404("Team does not exist")

    users = []
    by_user = defaultdict(dict)
    exercises = {}

    for user in User.objects.filter(teams=team).order_by("-points"):
        users.append(user)
        for exercise in Exercise.objects.with_user_stats(user):
            exercises[exercise.pk] = exercise
            by_user[user.pk][exercise.pk] = exercise

    exercises = sorted(exercises.values(), key=lambda exercise: exercise.position)

    table = []  # Table is one line per user, one column per exercise.
    for user in users:
        line = []
        for exercise in exercises:
            line.append(by_user[user.pk].get(exercise.pk))
        table.append(line)

    context = {
        "table": zip(users, table),
        "exercises": exercises,
        "team": team,
    }
    return render(request, "hkis/team_stats.html", context)


def teams_view(request):
    return render(
        request,
        "hkis/teams.html",
        {
            "teams": Team.objects.exclude(points__isnull=True)
            .order_by("-points")
            .select_related()
        },
    )


def team_view(request, slug):
    try:
        team = Team.objects.get(slug=slug)
    except Team.DoesNotExist:
        try:
            team = Team.objects.get(name=slug)
        except Team.DoesNotExist as err:
            raise Http404("Team does not exist") from err
        return redirect("team", slug=team.slug)
    requester_membership = None
    if not request.user.is_anonymous:
        with suppress(Membership.DoesNotExist):
            requester_membership = Membership.objects.get(team=team, user=request.user)
    context = {"team": team, "requester_membership": requester_membership}
    return render(request, "hkis/team.html", context)


def stats(request):
    my_exercises = Exercise.objects.with_monthly_stats(author=request.user)
    context = {"exercises": my_exercises}
    return render(request, "hkis/stats.html", context)

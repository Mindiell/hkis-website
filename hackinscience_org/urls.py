"""hkis URL Configuration."""

import debug_toolbar
from django.contrib import admin
from django.contrib.staticfiles import views
from django.urls import include, path, re_path

urlpatterns = [
    path("accounts/", include("registration.backends.simple.urls")),
    path("admin/pages/", include("hkis.admin")),
    path("admin/", admin.site.urls),
    path("__debug__/", include(debug_toolbar.urls)),
    path("", include("hkis.urls")),
    re_path(r"^static/(?P<path>.*)$", views.serve),
]
